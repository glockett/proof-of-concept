**Getting started

1. git clone https://gitlab.com/glockett/proof-of-concept.git
2. cd into proof-of-concept 
3. Add the temp license k3.lic file into the conf/service-manager-licenses folder.
4. Add the k4.lic file into the conf/data-server_licenses folder.
5. Run the ./docker-setup.sh 

Initially, Docker may ask to share folders. Accept it. 