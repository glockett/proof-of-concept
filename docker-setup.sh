#!/bin/sh

MSG1="Building and starting the containers..."
MSG2="Containers are running. Make sure you have valid licenses"

echo $MSG1
cd appian-docker
docker-compose build && docker-compose up -d
echo $MSG2

